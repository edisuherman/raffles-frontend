<title>Forget Password</title>
@extends('layouts.app')

@section('content')
@include('layouts.notification')
<div class="content-box content-home">
    <section class="content-blank gray-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8 offset-lg-2">
                    <div class="card">
                        <div class="card-body body-lg">
                        {!! Form::open(['url' => ['send_password'],'id'=>'send_password','method' => 'POST','enctype' => 'multipart/form-data']) !!}
                        @csrf
                            <h3 class="text-capitalize head-title mt-0 mb-2">Forgot Password?</h3>
                            <p class="head-sub-desc">Please Send Your Email</p>

                            <br>

                            @if(!session()->has('status_reg'))
                            @include('layouts.flash')
                            @endif

                            @include('layouts.flash_reg')


                            <div class="form-group">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email address" required data-validation-required-message="This field is required">
                                @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                            <br>
                            <button class="btn btn-primary btn-lg w-100" type="submit">Send</button>
                        {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
