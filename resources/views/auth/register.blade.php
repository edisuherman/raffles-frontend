<title>Register</title>
@extends('layouts.app')

@section('content')
@include('layouts.notification')
<div class="content-box content-home">
    <section class="content-blank gray-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8 offset-lg-2">
                    <div class="card">
                        <div class="card-body body-lg">
                            <h3 class="text-capitalize head-title mt-0 mb-2">Create My Account</h3>
                            <p class="head-sub-desc">Complete the form below</p>

                            @include('layouts.flash')

                            <br>
                            {!! Form::open(['url' => ['reg'],'id'=>'registrasi','method' => 'POST','enctype' => 'multipart/form-data']) !!}
                            <div class="form-group">
                                <label for="">Fullname</label>
                                <input type="text" name="fullname" id="fullname" value="{{$fullname}}" class="form-control" placeholder="Enter your name" aria-describedby="helpId" required data-validation-required-message="This field is required">
                            </div>

                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="email" name="email" id="email" value="{{$email}}" class="form-control" placeholder="example@mail.com" aria-describedby="helpId" required data-validation-required-message="This field is required">
                            </div>

                            <div class="form-group">
                                <label for="">Phone Number</label>
                                <input type="text" name="phone" id="phone" value="{{$phone}}" class="form-control" placeholder="08456456234" aria-describedby="helpId" required data-validation-required-message="This field is required">
                            </div>

                            <div class="form-group">
                                <label for="">Date of Birth</label>
                                <input type="date" name="birthday" id="birthday" value="{{$birthday}}" class="form-control" placeholder="10 Mei 1990" aria-describedby="helpId" required data-validation-required-message="This field is required">
                            </div>

                            <div class="form-group">
                                <label for="">Address</label>
                                <textarea name="address" class="form-control" placeholder="Enter address" id="address" cols="30" rows="10" required data-validation-required-message="This field is required">{{$address}}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="">Password</label>
                                <input type="password" name="password" id="password" class="form-control" placeholder="*******" aria-describedby="helpId" required data-validation-required-message="This field is required">
                            </div>

                            <div class="form-group">
                                <label for="">Re-type Password</label>
                                <input type="password" name="re_password" id="re_password" class="form-control" placeholder="*******" aria-describedby="helpId" required data-validation-required-message="This field is required">
                            </div>

                            <div>
                                <label class="customcheck m-t-10">I accept Royal Raffles Capital <a href="#" class="text-primary font-500">Terms of Service</a>
                                    <input type="checkbox" name="chk_term" required >
                                    <span class="checkmark"></span>
                                </label>
                            </div>

                            <div>
                                <label class="customcheck m-t-10">Subscribe Newsletter
                                    <input type="checkbox" name="chk_subscribe">
                                    <span class="checkmark"></span>
                                </label>
                            </div>

                            <br>

                            <button type="submit" class="btn btn-primary btn-lg w-100">Create Account</button>
                            {!! Form::close() !!}
                            <p class="mb-0 mt-4 text-center font-400">
                                Already have an account? <a href="/login" class="font-500 text-primary">
                                    Login now
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
