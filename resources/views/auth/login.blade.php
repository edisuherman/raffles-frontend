<title>Login</title>
@extends('layouts.app')

@section('content')
@include('layouts.notification')
<div class="content-box content-home">
    <section class="content-blank gray-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8 offset-lg-2">
                    <div class="card">
                        <div class="card-body body-lg">
                        <form class="form-horizontal form-material" id="loginform" method="POST" action="{{ route('login') }}">
                        @csrf
                            <h3 class="text-capitalize head-title mt-0 mb-2">Got an Account?</h3>
                            <p class="head-sub-desc">Login to your account</p>

                            <br>

                            @include('layouts.flash')
                            @include('layouts.flash_reg')


                            <div class="form-group">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email address" required data-validation-required-message="This field is required">
                                @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="d-flex align-items-center justify-content-between flex-wrap mb-3">
                                <div>
                                    <label class="customcheck m-t-10">Keep me signed in
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>

                                <div>
                                    <p class="m-0">
                                        <a href="/forgot_password" class="text-primary">Forgot Password?</a>
                                    </p>
                                </div>
                            </div>
                            <button class="btn btn-primary btn-lg w-100" type="submit">Yes, Login Now</button>

                            <p class="mb-0 mt-4 text-center font-400">
                                Don’t have account? <a href="/reg" class="font-500 text-primary">Register
                                    Now</a>
                            </p>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
