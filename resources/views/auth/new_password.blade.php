<title>Forget Password</title>
@extends('layouts.app')

@section('content')
@include('layouts.notification')
<div class="content-box content-home">
    <section class="content-blank gray-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8 offset-lg-2">
                    <div class="card">
                        <div class="card-body body-lg">
                        {!! Form::open(['url' => ['new_password'],'id'=>'new_password','method' => 'POST','enctype' => 'multipart/form-data']) !!}
                        @csrf
                            <h3 class="text-capitalize head-title mt-0 mb-2">New Password</h3>
                            <p class="head-sub-desc">Please Set Your New Password</p>

                            <br>
                            @if (\Session::has('failed'))
                                <div class="alert alert-danger">
                                    {{ session()->get('failed') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            <div class="form-group">
                                <input type="hidden" name='token_actived' value='{{$token_actived}}'>
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ old('password') }}" placeholder="Password" required data-validation-required-message="This field is required">
                                @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                             <div class="form-group">
                                <input id="conf_password" type="password" class="form-control{{ $errors->has('conf_password') ? ' is-invalid' : '' }}" name="conf_password" value="{{ old('conf_password') }}" placeholder="Confirm Password" required data-validation-required-message="This field is required">
                                @if ($errors->has('conf_password'))
                                <span class="invalid-feedback">
                                <strong>{{ $errors->first('conf_password') }}</strong>
                                </span>
                                @endif
                            </div>
                            <br>
                            <button class="btn btn-primary btn-lg w-100" type="submit">Submit</button>
                        {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
