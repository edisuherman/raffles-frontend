<title>Contact Us</title>
@extends('layouts.app')

@section('content')
<div class="content-box content-home">
    <section class="sub-header pb-0">
        <div class="block-over-img card-image-only">
            <img class="img-bg img-fluid w-100" src="{{asset('assets/images/pictures/'.$header->name)}}">

            <div class="center-left w-100">
                <div class="text-block">
                    <h2 class="text-capitalize head-title text-white font-600 mb-2">Contact Us</h2>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li><a href="#">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-blank">
        <div class="container">
            <h3 class="text-capitalize head-title">Let Us Know What You Have In Mind</h3>
            <p class="head-sub-desc">Visit our shop to see amazing creations from our designers</p>

            <div class="row m-t-70">
                <!-- <div class="col-12 col-md-6 col-lg-3 mb-4">
                    <div class="card-body p-0">
                        <div class="card-center-absolute">
                            <div class="d-flex">
                                <div class="mr-3">
                                    <i class="fa fa-map-marker fa-2x text-gold" aria-hidden="true"></i>
                                </div>

                                <div>
                                    <div class="card-title text-default mb-2">Store Location:</div>
                                    <p class="font-300 mb-0">
                                        <?php
				                        	echo $company->address;
				                        ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->

                <div class="col-12 col-md-6 col-lg-3 mb-4">
                    <div class="card-body p-0">
                        <div class="card-center-absolute">
                            <div class="d-flex">
                                <div class="mr-3">
                                    <i class="fa fa-phone fa-2x text-gold" aria-hidden="true"></i>
                                </div>

                                <div>
                                    <div class="card-title text-default mb-2">Call Number:</div>
                                    <p class="font-300 mb-0">
                                        <?php
				                        	echo $company->phone;
				                        ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-3 mb-4">
                    <div class="card-body p-0">
                        <div class="card-center-absolute">
                            <div class="d-flex">
                                <div class="mr-3">
                                    <i class="fa fa-envelope fa-2x text-gold" aria-hidden="true"></i>
                                </div>

                                <div>
                                    <div class="card-title text-default mb-2">Email</div>
                                    <p class="font-300 mb-0">
                                        <?php
				                        	echo $company->email;
				                        ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-3 mb-4">
                    <div class="card-body p-0">
                        <div class="card-center-absolute">
                            <div class="d-flex">
                                <div class="mr-3">
                                    <i class="fa fa-clock-o fa-2x text-gold" aria-hidden="true"></i>
                                </div>

                                <div>
                                    <div class="card-title text-default mb-2">Working Hours</div>
                                    <p class="font-300 mb-0">
                                        <?php
				                        	echo $company->working_hours;
				                        ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-blank gray-section">
        <div class="container mb-4">
            <!-- <div class="row">
                <div class="col-12 col-md-6 mb-4">
                    <div class="mapouter">
                        <div class="mapouter">
                            <div class="gmap_canvas">
                            	<?php
                            		$address_temp =  $company->address;
                            		$address = str_replace(' ', '%20', $address_temp); 
                                    $lat = $company->latitude;
                                    $long = $company->longitude;
                            	?>
                                <iframe width="525" height="500" id="gmap_canvas"
                                    src="https://maps.google.com/maps?q=<?php echo $address;?>&t=&z=19&ie=UTF8&iwloc=&output=embed"
                                    frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                                <iframe width="525" height="500" id="gmap_canvas"
                                    src="https://maps.google.com/maps?q=<?php echo $lat; ?>,<?php echo $long; ?>&hl=es;z=14&amp;output=embed"
                                    frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                            </div>
                        </div>
                    </div>
                </div> -->
                
				      
				    

                <div class="col-12 col-md-6 mb-4">
                    @if(session()->has('status_contact'))
                	@include('layouts.flash')
                    @endif
                    <h5 class="text-capitalize head-title mt-0 mb-2">Contact</h5>
                    <p class="head-sub-desc mb-2">Your email address will not be published</p>

                    <br>
                    {!! Form::open(['url' => ['frontend/contact_us'],'id'=>'contact_us','method' => 'POST','enctype' => 'multipart/form-data']) !!}

                    <div class="form-group">
                        <label for="">Fullname</label>
                        <input type="text" name="fullname" id="fullname" class="form-control" placeholder="Your Name"
                            aria-describedby="helpId" required data-validation-required-message="This field is required">
                    </div>

                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="email" name="email" id="email" class="form-control" placeholder="example@mail.com"
                            aria-describedby="helpId" required data-validation-required-message="This field is required">
                    </div>

                    <div class="form-group">
                        <label for="">Subject</label>
                        <input type="text" name="subject" id="subject" class="form-control" placeholder="Subject"
                            aria-describedby="helpId" required data-validation-required-message="This field is required">
                    </div>

                    <div class="form-group">
                        <label for="">Message</label>
                        <textarea name="message" class="form-control" rows="5" placeholder="Your Messege" required></textarea>
                    </div>

                    <br>

                    <button type="submit" class="btn btn-primary">Send Message</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
