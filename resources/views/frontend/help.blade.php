<title>Help Center</title>
@extends('layouts.app')

@section('content')
<div class="content-box content-home static-page">
    <section class="sub-header pb-0">
        <div class="block-over-img card-image-only">
            <img class="img-bg img-fluid w-100" src="{{asset('assets/images/pictures/'.$header->name)}}">

            <div class="center-left w-100">
                <div class="text-block">
                    <h2 class="text-capitalize head-title text-white font-600 mb-2">{{$help_first->name}}</h2>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li><a href="#">{{$help_first->name}}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-blank">
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <h3 class="text-gold font-title mb-2">{{$help_first->name}}</h3>
                    <hr>
                    <?php echo $help_first->description;?>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
