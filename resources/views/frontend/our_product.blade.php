<title>Our Product</title>
@extends('layouts.app')

@section('content')
<div class="content-box content-home">
    <section class="sub-header pb-0">
        <div class="block-over-img card-image-only">
            <img class="img-bg img-fluid w-100" src="{{asset('assets/images/pictures/'.$header->name)}}">

            <div class="center-left w-100">
                <div class="text-block">
                    <h2 class="text-capitalize head-title text-white font-600 mb-2">Our Product</h2>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li><a href="#">Our Product</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-blank gray-section">
        <div class="container">
            <div class="row">
                <div id="ProductCategorySection" class="col-12 col-lg-4 mb-4">
                    <h5 class="text-capitalize head-title mt-0 mb-3">Search</h5>
                     {!! Form::open(['url' => ['frontend/our_product'],'id'=>'txtsearch','method' => 'POST','enctype' => 'multipart/form-data']) !!}
                    <div class="input-group">
                        <input type="text" name="txtsearch" id="txtsearch" class="form-control"
                                value="{{$search_text}}" required data-validation-required-message="This field is required">
                        <div class="input-group-append">
                            <button type="submit" class="btn bg-white">
                                <i class="fa fa-search text-secondary"></i>
                            </button>
                        </div>
                    </div>
                     {!! Form::close() !!}

                    <hr class="mt-4 mb-4">

                    <div>
                        <h5 class="text-capitalize head-title mt-0 mb-3">Product Categories</h5>

                        <ul class="nav category tab-vertical flex-column" id="myTab" role="tablist">
                            <?php
                            $vtp_name="";
                            if (!$vtp)
                            {
                                $vtp_name = "active";
                            }
                            ?>
                            <li class="nav-item">
                                <a class="{{$vtp_name}}" id="VTP_All"  href="/frontend/our_product/" role="tab" aria-controls="VTP_All_Content" aria-selected="true">
                                    <p>All Category </p>
                                </a>
                            </li>

                            @foreach ($type_product as $data )

                            <li class="nav-item">
                                @if ($data->id==$id)
                                <a class="active" id="VTP_{{ str_replace(' ','',$data->name) }}"  href="/frontend/our_product/{{$data->name.'-'.$data->id}}/" role="tab" aria-controls="VTP_{{ str_replace(' ','',$data->name) }}_Content" aria-selected="false">
                                    <p>{{ $data->name }}</p>
                                </a>
                                @else
                                <a id="VTP_{{ str_replace(' ','',$data->name) }}"  href="/frontend/our_product/{{$data->name.'-'.$data->id}}/" role="tab" aria-controls="VTP_{{ str_replace(' ','',$data->name) }}_Content" aria-selected="false">
                                    <p>{{ $data->name }}</p>
                                </a>
                                @endif
                            </li>

                            @endforeach
                        </ul>
                    </div>
                </div>

                <?php
                $tags = explode('/' , request()->path()); 
                $jmltags = count($tags);
                $page='';
                $link='';
                if ($jmltags>2)
                    {
                        $page     = $tags[2]; // piece2
                        $link     = "/frontend/our_product/".$page."/";
                    }
                if ($search_text!='Type Keyword')
                    {
                        $union    = $category_name.'-'.$id;
                        $page     = str_replace(' ','%20',$union); // piece2
                        $link     = "/frontend/our_product/".$page."/";
                    }
                $search_text = str_replace(' ','%20',$search_text);
                //echo $category_name.'-'.$jmltags.'-'.$page;
                $s1='';
                $s2='';
                $s3='';
                if ($id_sorting==1)
                    {
                        $s1 = "selected";
                    }
                if ($id_sorting==2)
                    {
                        $s2 = "selected";
                    }
                if ($id_sorting==3)
                    {
                        $s3 = "selected";
                    }
                ?>

                <div id="ProductListSection" class="col-12 col-lg-8 mb-4">
                    <div class="product-line-filter">
                        <p class="result mb-0">Showing all {{$product->total()}} results</p>
                        <div class="form-group sort mb-0">
                            <select name="" id="" class="form-control w-auto filter-select-option" style="text-indent:1px" onchange="if (this.value) window.location.href=this.value"> 
                                <?php if ($jmltags>=3 and $page!='order' and $category_name !='All Category') {?> 
                                <option value=<?php echo $link.'order/0_'.$search_text;?> >Default Sorting</option>
                                <option {{$s1}} value=<?php echo $link.'order/1_'.$search_text;?> >Newest</option>
                                <option {{$s2}} value=<?php echo $link.'order/2_'.$search_text;?> >Price: Low to High</option>
                                <option {{$s3}} value=<?php echo $link.'order/3_'.$search_text;?> >Price: High to Low</option>
                                <?php } elseif($page!='order' and $search_text!='' and $category_name =='All Category'){?>
                                <option value=<?php echo '/frontend/our_product/order/0_'.$search_text ?> >Default Sorting</option>
                                <option {{$s1}} value=<?php echo '/frontend/our_product/order/1_'.$search_text ?>>Newest</option>
                                <option {{$s2}} value=<?php echo '/frontend/our_product/order/2_'.$search_text ?>>Price: Low to High</option>
                                <option {{$s3}} value=<?php echo '/frontend/our_product/order/3_'.$search_text ?>>Price: High to Low</option>
                                <?php } elseif($page!='order' and $search_text!=''){?>
                                <option value=<?php echo $link.'order/0_'.$search_text;?> >Default Sorting</option>
                                <option {{$s1}} value=<?php echo $link.'order/1_'.$search_text;?> >Newest</option>
                                <option {{$s2}} value=<?php echo $link.'order/2_'.$search_text;?> >Price: Low to High</option>
                                <option {{$s3}} value=<?php echo $link.'order/3_'.$search_text;?> >Price: High to Low</option>
                                <?php } else {?>
                                <option value=<?php echo '/frontend/our_product/order/0_'.$search_text ?> >Default Sorting</option>
                                <option {{$s1}} value=<?php echo '/frontend/our_product/order/1_'.$search_text ?>>Newest</option>
                                <option {{$s2}} value=<?php echo '/frontend/our_product/order/2_'.$search_text ?>>Price: Low to High</option>
                                <option {{$s3}} value=<?php echo '/frontend/our_product/order/3_'.$search_text ?>>Price: High to Low</option>
                                <?php } ?>
                            </select>
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </div>

                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="VTP_{{$vtp}}_Content" role="tabpanel" aria-labelledby="VTP_{{$vtp}}">
                            <p class="font-500 text-primary">{{$category_name}}</p>
                            <div class="row">
                            @foreach ($product as $data )
                                <div class="col-12 col-sm-6 col-lg-4 mb-4">
                                    <div class="card">
                                        <div class="block-over-img img-lg">
                                            <img class="card-img-top img-fluid w-100" src="{{asset('assets/images/pictures/'.$data->image_1)}}" >
                                        </div>
                                        <div class="card-body">
                                            <p class="card-text text-secondary">{{ $data->type_product }}</p>
                                            <p class="text-default mb-2">{{ $data->name }}</p>
                                            <p class="card-text text-gold font-300">Rp {{ number_format($data->price) }}</p>
                                            <hr>
                                            <p class="card-text text-default text-center">
                                                <a href="/frontend/our_product/detail/{{$data->id}}" class="text-default" tabindex="0">
                                                    <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                                    Detail
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </div>
                        </div>
                    </div>

                    <br>
                    
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center align-items-center w-100">
                            {{ $product->links('vendor.pagination.custom') }}
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
