@extends('layouts.app')

@section('content')
<meta property="og:url"           content= <?php echo $_SERVER['REQUEST_URI'];?> />
<meta property="og:type"          content="website" />
<meta property="og:title"         content="{{$news_view->name}}" />
<meta property="og:description"   content="{{$news_view->short_description}}" />
<meta property="og:image"         content="{{asset('assets/images/pictures/'.$news_view->image)}}" />

<div class="content-box content-home">
    <section class="sub-header pb-0">
        <div class="block-over-img card-image-only">
            <img class="img-bg img-fluid w-100" src="{{asset('assets/images/pictures/'.$header->name)}}">

            <div class="center-left w-100">
                <div class="text-block">
                    <h2 class="text-capitalize head-title text-white font-600 mb-2">News</h2>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li><a href="/frontend/news/">News</a></li>
                            <li><a href="#">{{$news_view->name}}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-blank gray-section">
        <div class="row mr-auto">
            <div class="col-12 col-lg-8 mb-4">
                <div class="card" style="border-left: 0 !important;">
                    <div class="block-over-img img-xxl">
                        <img class="card-img-top img-fluid w-100" src="{{asset('assets/images/pictures/'.$news_view->image)}}">
                    </div>
                    <div class="read-article mb-1">
                        <h1 class="title-article">{{$news_view->name}}</h1>
                        <p class="date-post">{{date("F d, Y",strtotime($news_view->created_at))}}</p>
                        <?php echo $news_view->description ?>
                    </div>
                    
                    <hr>
                    <?php $link= $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>
                    <div class="read-article">
                        <h5 class="text-capitalize head-title mt-0 mb-3">Share Article</h5>
                        <div class="mb-4">
                          <!-- <div class="fb-share-button" 
                            data-href=<?php echo $link;?> 
                            data-layout="button" data-size="large">
                          </div> -->
                            <a href="https://www.facebook.com/sharer/sharer.php?u={{$link}}" target="_blank" class="btn btn-fb mr-2">
                                <i class="fa fa-facebook-square fa-lg mr-2" aria-hidden="true"></i> Facebook
                            </a>
                            <a href="http://twitter.com/share?text=check this news : {{$link}} &url={{$link}}" class="btn btn-twitter mr-2" target="_blank">
                                <i class="fa fa-twitter fa-lg mr-2" aria-hidden="true"></i> Twitter
                            </a>

                            <a href="https://plus.google.com/share?url={{$link}}" class="btn btn-google-plus mr-2" target="_blank">
                                <i class="fa fa-google-plus fa-lg mr-2" aria-hidden="true"></i> Google+
                            </a>
                        </div>

                        <br>

                        <h5 class="text-capitalize head-title mt-0 mb-4">All Comment</h5>
                        <ul class="comment-list">
                            <li>
                                @foreach ($news_comment as $data )
                                <div class="card-author">
                                    <img src="{{asset('assets/images/pictures/nopic.png')}}" class="img-fluid img-profile img-rounded mr-4" alt="">
                                    <div class="info-profile">
                                        <p class="title title-font-500 mb-1">{{$data->fullname}}</p>
                                        <p class="text-gold mb-2">{{date("F d, Y",strtotime($data->created_at))}}</p>
                                        <p>
                                            {{$data->description}} 
                                        </p>
                                    </div>
                                </div>
                                @endforeach
                            </li>
                        </ul>

                        <br>

                        <div class="row">
                            <div class="col-12 col-md-10 mb-4">
                                <h5 class="text-capitalize head-title mt-0 mb-2">Leave a Reply</h5>
                                <p class="head-sub-desc mb-2">Your email address will not be published</p>

                                <br>
                                @if(Auth::user())

                                {!! Form::open(['url' => ['frontend/news/comment'],'id'=>'comment','method' => 'POST','enctype' => 'multipart/form-data']) !!}

                                @if(session()->has('status_contact'))
                                @include('layouts.flash')
                                @endif

                                <div class="form-group">
                                    <label for="">Fullname</label>
                                    <input type="hidden" name="id_news" id="" value="{{$id}}" class="form-control" placeholder="Your Name" aria-describedby="helpId" readonly="readonly">
                                     <input type="hidden" name="id_user" id="" value="{{Auth::user()->id}}" class="form-control" placeholder="Your Name" aria-describedby="helpId" readonly="readonly">
                                    <input type="text" name="fullname" id="" value="{{Auth::user()->name}}" class="form-control" placeholder="Your Name" aria-describedby="helpId" readonly="readonly">
                                </div>

                                <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="email" name="email" id="" value="{{Auth::user()->email}}" class="form-control" placeholder="example@mail.com" aria-describedby="helpId" readonly="readonly">
                                </div>


                                <div class="form-group">
                                    <label for="">Message</label>
                                    <textarea class="form-control" name="message" rows="5" placeholder="Your Message"></textarea>
                                </div>

                                <br>

                                <button type="submit" class="btn btn-primary btn-lg">Send Comment</button>
                                {!! Form::close() !!}
                                @else

                                <a href="/login" class="btn btn-primary btn-lg">Login</a>

                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <h5 class="text-capitalize head-title mt-0 mb-3">Search</h5>
                    {!! Form::open(['url' => ['frontend/news'],'id'=>'txtsearch','method' => 'POST','enctype' => 'multipart/form-data']) !!}
                    <div class="input-group">
                        <input type="text" name="txtsearch" id="txtsearch" class="form-control"
                                value="{{$search}}" required data-validation-required-message="This field is required">
                        <div class="input-group-append">
                            <button type="submit" class="btn bg-white">
                                <i class="fa fa-search text-secondary"></i>
                            </button>
                        </div>
                    </div>
                     {!! Form::close() !!}

                <hr class="mt-4 mb-4">

                <div>
                    <h5 class="text-capitalize head-title mt-0 mb-3">Recent Pos</h5>
                        @foreach ($news_recent as $data )
                        <div class="card-author mb-4">
                            <img src="{{asset('assets/images/pictures/'.$data->image)}}" class="img-fluid img-profile" alt="">
                            <div class="info-profile">
                                <p class="title"><a href="/frontend/news/detail/{{$data->id}}">{{$data->name}}</a></p>
                                <div class="d-flex align-items-center">
                                    <p class="desc mr-3"><a href="/frontend/news/category_news/{{$data->id_category_news}}" class="text-red text-uppercase font-500">{{$data->category_name}}</a></p>
                                    <p class="desc text-uppercase">{{date("F d, Y",strtotime($data->created_at))}}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                </div>

                <hr class="mt-4 mb-4">

                <div>
                    <h5 class="text-capitalize head-title mt-0 mb-3">Category</h5>
                    <?php 
                            $active_all = '' ;
                            if ($id==0)
                            {
                                $active_all = 'class="active"';
                            }
                        ?>
                        <ul class="category">
                            <li <?php echo $active_all; ?> >
                                <a href="/frontend/news/">
                                    <p>All</p>
                                    <p style="width:30px">{{'( '.$news_count.' )'}}</p>
                                </a>
                            </li>
                            @foreach ($type_news as $data )
                            <li <?php
                                if ($data->id == $id)
                                {
                                    echo "class='active'";
                                }
                                ?> >
                                <a href="/frontend/news/category_news/{{$data->id}}">
                                    <p>{{$data->name}}</p>
                                    <p style="width:30px">{{'( '.$data->sum.' )'}}</p>
                                </a>
                            </li>
                            @endforeach
                        </ul>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- Load Facebook SDK for JavaScript -->
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

@endsection
