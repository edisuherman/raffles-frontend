@extends('layouts.app')

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>

@section('content')
<div class="content-box content-home">
    <section class="sub-header pb-0">
        <div class="block-over-img card-image-only">
            <img class="img-bg img-fluid w-100" src="{{asset('assets/images/pictures/'.$header->name)}}">

            <div class="center-left w-100">
                <div class="text-block">
                    <h2 class="text-capitalize head-title text-white font-600 mb-2">Live Pricing</h2>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><a href="#">Live Pricing</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-blank gray-section">
        <div class="container">
            <div class="row mb-2">
                <div class="col-12 col-lg-6 mb-4">
                    <div class="card">
                        <div class="card-body anual-pricing">
                            <h6 class="font-desc text-gold font-500 mb-3">Buy XAU/IDR</h6>
                            <h3 class="font-desc text-default font-500 mb-0">
                                638, 693, 900.00
                                <span class="status-pricing text-primary"><i class="fa fa-caret-down" aria-hidden="true"></i> 18, 600.00</span>
                            </h3>

                            <hr>

                            <div class="d-flex flex-wrap justify-content-between">
                                <div>
                                    <span class="status-pricing font-desc">Sell USD/IDR</span>
                                    <h5 class="font-desc text-red font-500 mt-2 mb-0">14,147.00</h5>
                                </div>

                                <div class="text-right">
                                    <span class="status-pricing font-desc">Ask XAU/USD</span>
                                    <h5 class="font-desc text-red font-500 mt-2 mb-0">1,529.51</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-6 mb-4">
                    <div class="card">
                        <div class="card-body anual-pricing">
                            <h6 class="font-desc text-gold font-500 mb-3">Sell XAU/IDR</h6>
                            <h3 class="font-desc text-default font-500 mb-0">
                                634, 772, 700.00
                                <span class="status-pricing text-primary"><i class="fa fa-caret-down" aria-hidden="true"></i> 55, 400.00</span>
                            </h3>

                            <hr>

                            <div class="d-flex flex-wrap justify-content-between">
                                <div>
                                    <span class="status-pricing font-desc">Buy USD/IDR</span>
                                    <h5 class="font-desc text-red font-500 mt-2 mb-0">14,147.00</h5>
                                </div>

                                <div class="text-right">
                                    <span class="status-pricing font-desc">Bid XAU/USD</span>
                                    <h5 class="font-desc text-red font-500 mt-2 mb-0">1,529.51</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="card">
               <div id="container" style="min-width: 1080px; height: 400px; margin: 0 auto"></div>
            </div>
        </div>
    </section>
</div>


<input class="ld-time-input" type="hidden" value="2" id="pollingTime"/>
<input class="ld-url-input" type="hidden" id="fetchURL"/>

<script type="text/javascript">
var defaultData = 'https://demo-live-data.highcharts.com/time-data.csv';
var urlInput = document.getElementById('fetchURL');
//var pollingCheckbox = document.getElementById('enablePolling');
var pollingInput = document.getElementById('pollingTime');

function createChart() {
    Highcharts.chart('container', {
        chart: {
            type: 'spline'
        },
        title: {
            text: 'XAU/IDR'
        },
        xAxis: {
                title: {
                    text: 'Date and Time'
                }
            },
            yAxis: {
                title: {
                    text: 'Exchange Rate'
                }
            },
        data: {
            csvURL: urlInput.value,
            enablePolling: true,
            dataRefreshRate: parseInt(pollingInput.value, 10)
        }
    });

    // if (pollingInput.value < 1 || !pollingInput.value) {
    //     pollingInput.value = 1;
    // }
}

urlInput.value = defaultData;

// We recreate instead of using chart update to make sure the loaded CSV
// and such is completely gone.
//pollingCheckbox.onchange = urlInput.onchange = pollingInput.onchange = createChart;

// Create the chart
createChart();
</script>
@endsection
