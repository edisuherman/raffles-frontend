<title>Home</title>
@extends('layouts.app')

@section('content')
<div class="content-box content-home">
	<header id="headerSection" class="home-section">
		<div id="demo" class="carousel slide" data-ride="carousel" data-interval="3000">
			<span class="d-none d-md-block">
				<ul class="carousel-indicators">
					<li data-target="#demo" data-slide-to="0" class="active"></li>
					<li data-target="#demo" data-slide-to="1"></li>
					<li data-target="#demo" data-slide-to="2"></li>
				</ul>
			</span>

			<div class="carousel-inner">
				<?php $a=1;?>
				@foreach ($slider as $data )
				<?php
						
						if ($a==1)
						{
							$active_slider ="carousel-item active";
						}
						else
						{
							$active_slider ="carousel-item";
						}
					?> 
				<div class='<?php echo $active_slider; ?>'>
					<div class="dark-img">
						<img class="d-block w-100" src="{{asset('assets/images/pictures/'.$data->image)}}" alt="first slide">
					</div>
					<div class="carousel-caption container d-flex align-items-center">
						<div class="text-block">
							<?php echo $data->description; ?>
							<a href="{{$data->link}}" class="btn btn-primary btn-lg">View Our Collection</a>
						</div>
					</div>
				</div>
				<?php $a++;?>
				@endforeach
			</div>
		</div>
	</header>

	<section class="content-blank pb-0">
		<div class="container">
			<h3 class="text-capitalize head-title">Why Royal Raffles Capital?</h3>
			<!-- <p class="head-sub-desc">Lorem ipsum dolor sita met qonqueror</p> -->

			<div class="row m-t-70 justify-content-center">
				<div class="col-12 col-sm-6 col-md-4 mb-4">
					<div class="card-body text-center">
						<div class="card-center-absolute">
							<img src="assets/images/icons/gold-pyramid.png" width="75"
								class="img-fluid icon-featured mb-4" alt="">
							<div class="card-title mb-3">Gold 99.99%</div>
							<!-- <p class="font-300 mb-0">
								We guarantee the authenticity of the product and the purity of 99.99%
							</p> -->
						</div>
					</div>
				</div>

				<!-- <div class="col-12 col-sm-6 col-md-4 mb-4">
					<div class="card-body text-center">
						<div class="card-center-absolute">
							<img src="assets/images/icons/safe-box.png" width="75"
								class="img-fluid icon-featured mb-4" alt="">
							<div class="card-title mb-3">Safe Box Service</div>
							<p class="font-300 mb-0">
								The smart way to invest gold without risk is lost, with the purchase price of gold
								that is cheaper
							</p>
						</div>
					</div>
				</div> -->

				<div class="col-12 col-sm-6 col-md-4 mb-4">
					<div class="card-body text-center">
						<div class="card-center-absolute">
							<img src="assets/images/icons/credit-card.png" width="75"
								class="img-fluid icon-featured mb-4" alt="">
							<div class="card-title mb-3">Secure Transaction</div>
							<!-- <p class="font-300 mb-0">
								Website security is equipped with SSL Certificate encryption.
							</p> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="content-blank">
		<div class="container mb-4">
			<h3 class="text-capitalize head-title">Our Product</h3>
			<!-- <p class="head-sub-desc">Lorem ipsum dolor sita met qonqueror</p> -->

			<nav class="m-t-40 mb-2">
				<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
					<?php $i=1;?>
					@foreach ($type_product as $data )
					<?php
						
						if ($i==1)
						{
							$active ="nav-item text-uppercase active";
						}
						else
						{
							$active ="nav-item text-uppercase";
						}
					?> 
					<a class='<?php echo $active; ?>' data-toggle="tab"
						href="#navTabs{{str_replace(' ','',$data->name)}}">{{$data->name}}</a>

					<?php $i++;?>
					@endforeach
				</div>
			</nav>

			<div class="tab-content py-3" id="nav-tabContent">
				<?php $x=1;?>
				@foreach ($type_product as $data)
				<?php
						
						if ($x==1)
						{
							$active_tab ="tab-pane fade active show";
						}
						else
						{
							$active_tab ="tab-pane fade";
						}
				?> 
				<div class='<?php echo $active_tab; ?>' id="navTabs{{str_replace(' ','',$data->name)}}">
					<div class="slick_best-product_{{$x}} slick-custom">
						@foreach ($product as $data_product )
						@if ($data->id == $data_product->id_category)
						<div class="card card-slick-custom">
							<div class="block-over-img img-lg">
								<img class="card-img-top img-fluid w-100" src="{{asset('assets/images/pictures/'.$data_product->image_1)}}">
							</div>
							<div class="card-body">
								<p class="card-text text-secondary">{{$data_product->type_product}}</p>
								<p class="text-default mb-2">{{$data_product->name}}</p>
								<p class="card-text text-gold font-300">Rp {{ number_format($data_product->price) }}</p>
								<hr>
								<p class="card-text text-default text-center">
									<a href="/frontend/our_product/detail/{{$data_product->id}}" class="text-default">
										<i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
										Detail
									</a>
								</p>
							</div>
						</div>
						@endif
						@endforeach	
					</div>
				</div>
				<?php $x++;?>
				@endforeach	
			</div>
		</div>
	</section>

	<section class="content-blank gray-section">
		<div class="container mb-4">
			<h3 class="text-capitalize head-title">News</h3>
			<!-- <p class="head-sub-desc">Lorem ipsum dolor sita met qonqueror</p> -->

			<div class="slick_home-news slick-custom m-t-40">
				@if($news_recent)
				@foreach ($news_recent as $data )
				<div class="card card-slick-custom">
					<div class="block-over-img img-lg">
						<img class="card-img-top img-fluid w-100" src="{{asset('assets/images/pictures/'.$data->image)}}">
					</div>
					<div class="card-body">
						<h6 class="text-gold font-title mb-2">{{$data->name}}</h6>
						<p class="card-text text-default">{{date("F d, Y",strtotime($data->created_at))}}</p>
						<p class="card-text font-300">
							{{$data->short_description}}
						</p>
						<br>
						<a href="/frontend/news/detail/{{$data->id}}" class="btn btn-primary">Read More</a>
					</div>
				</div>
				@endforeach
				@endif
			</div>

			<div class="text-center m-t-100">
				<a href="/frontend/news/" class="btn btn-primary btn-lg">Show More News</a>
			</div>
		</div>
	</section>
</div>
@endsection
