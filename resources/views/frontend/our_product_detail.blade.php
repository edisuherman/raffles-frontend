<title>Our Product Detail</title>
@extends('layouts.app')

@section('content')
<div class="content-box content-home">
    <section class="sub-header pb-0">
        <div class="block-over-img card-image-only">
            <img class="img-bg img-fluid w-100" src="{{asset('assets/images/pictures/'.$header->name)}}">

            <div class="center-left w-100">
                <div class="text-block">
                    <h2 class="text-capitalize head-title text-white font-600 mb-2">Our Product</h2>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><a href="/frontend/our_product">Our Product</a></li>
                            <li><a href="#"><?php echo $product->name;?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-blank gray-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-5">
                    <div class="card">
                        <div class="card-body">
                            <div class="easyzoom easyzoom--overlay easyzoom--with-thumbnails">
                                <a href="{{asset('assets/images/pictures/'.$product->image_1)}}">
                                    <img src="{{asset('assets/images/pictures/'.$product->image_1)}}" alt="" width="640" height="400" />
                                </a>
                            </div>

                            <ul class="thumbnails">
                                <?php  for( $i= 0 ; $i <= 5 ; $i++ )
                                {
                                    $image = 'image_'.$i;
                                ?>
                                    @if ($product->$image)
                                    <li>
                                        <a href="{{asset('assets/images/pictures/'.$product->image)}}" data-standard="{{asset('assets/images/pictures/'.$product->$image )}}">
                                            <img src="{{asset('assets/images/pictures/'.$product->$image )}}" alt="" />
                                        </a>
                                    </li>
                                    @endif
                                <?php }?>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-7">
                    <div class="card-body">
                        <p class="card-text text-secondary mb-1"><?php echo $product->type_product;?></p>
                        <h2 class="text-default mb-4"><?php echo $product->name; ?></h2>
                        <p class="card-text font-300">
                            <?php echo $product->description;?>
                        </p>
                        <br>
                        <h6 class="card-text font-desc text-gold font-500">Price</h6>
                        <h3 class="card-text font-desc text-default font-500">Rp. <?php echo number_format($product->price);?></h3>
                        @if ($product->information)
                        <div class="block-rules">
                            <div class="rule-icon"><i class="fa fa-info" aria-hidden="true"></i></div>
                            <div class="rule-text">
                                <?php echo $product->information; ?>  
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-blank">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-12 col-lg-7">
                    <h3 class="text-capitalize head-title mb-4">Product Info</h3>
                    <?php echo $product->product_info; ?>
                </div>

                <div class="col-12 col-lg-4">
                    <h3 class="text-capitalize head-title mb-4">Spesification</h3>
                    <?php echo $product->spesification; ?>
                </div>
            </div>
        </div>
    </section>
    
    <section class="content-blank gray-section pb-6">
        <div class="container">
            <h3 class="text-capitalize head-title">Related Product</h3>
            <p class="head-sub-desc">Lorem ipsum dolor sita met qonqueror Lorem ipsum dolor sita met qonqueror</p>

            <div class="slick_related-product slick-custom mt-4">
                @foreach ($product_all as $dataproduct)
                    @if ($dataproduct->id <> $product->id and $dataproduct->id_category == $product->id_category)
                    <div class="card card-slick-custom">
                        <div class="block-over-img img-lg">
                            <img class="card-img-top img-fluid w-100" src="{{asset('assets/images/pictures/'.$dataproduct->image_1)}}">
                        </div>
                        <div class="card-body">
                            <p class="card-text text-secondary">{{$dataproduct->type_product}}</p>
                            <p class="text-default mb-2">{{$dataproduct->name}}</p>
                            <p class="card-text text-gold font-300">Rp {{number_format($dataproduct->price)}}</p>
                            <hr>
                            <p class="card-text text-default text-center">
                                <a href="/frontend/our_product/detail/{{ $dataproduct->id }}" class="text-default">
                                    <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                    Detail
                                </a>
                            </p>
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
    </section>
</div>
@endsection
