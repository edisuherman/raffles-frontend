<title>Searching</title>
@extends('layouts.app')

@section('content')
<div class="content-box content-home static-page">
    <section class="sub-header pb-0">
        <div class="block-over-img card-image-only">
            <img class="img-bg img-fluid w-100" src="{{asset('assets/images/pictures/'.$header->name)}}">

            <div class="center-left w-100">
                <div class="text-block">
                    <h2 class="text-capitalize head-title text-white font-600 mb-2">Search Results</h2>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li><a href="#">Search "{{$name_request}}"</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-blank">
        <div class="container">
            <h4 class="text-gold font-title mb-2">Results for "{{$name_request}}"</h4>
            <p class="card-text font-300">
                We found <strong>{{$count_all}} search results</strong> for keyword "<strong class="text-gold">{{$name_request}}</strong>"
            </p>

            <br>

            <div class="card mb-4">
                <div class="card-body">
                    <h3 class="text-gold font-title mb-2">Product</h3>
                    <div class="row mt-4">
                        @foreach ($product as $data )
                        <div class="col-12 col-sm-6 col-lg-3 mb-4">
                            <div class="card card-product">
                                <div class="block-over-img img-lg">
                                    <img class="card-img-top img-fluid w-100" src="{{asset('assets/images/pictures/'.$data->image_1)}}">
                                </div>
                                <div class="card-body">
                                    <div class="content-list">
                                        <p class="card-text text-secondary">{{$data->type_product}}</p>
                                        <p class="text-default mb-2">{{$data->name}}</p>
                                        <p class="card-text text-gold font-300">Rp {{ number_format($data->price) }}</p>
                                    </div>
                                    <hr>
                                    <p class="card-text text-default text-center mb-0">
                                        <a href="/frontend/our_product/detail/{{$data->id}}" class="text-default" tabindex="0">
                                            <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                            Detail
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <h3 class="text-gold font-title mb-2">News</h3>
                    <div class="row mt-4">
                        @foreach ($news as $data )
                        <div class="col-12 col-md-4 mb-4">
                            <div class="card">
                                <div class="block-over-img img-lg">
                                    <img class="card-img-top img-fluid w-100" src="{{asset('assets/images/pictures/'.$data->image)}}">
                                    <div class="top-left">
                                        <span class="badge badge-secondary">{{$data->category_name}}</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <h5 class="text-gold font-title mb-2">{{$data->name}}</h5>
                                    <p class="card-text text-default">{{date("F d, Y",strtotime($data->created_at))}}</p>
                                    <p class="card-text font-300">
                                        {{$data->short_description}}
                                    </p>
                                    <br>
                                    <a href="/frontend/news/detail/{{$data->id}}" class="btn btn-primary" tabindex="0">Read More</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
