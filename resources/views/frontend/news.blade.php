@extends('layouts.app')

@section('content')
<div class="content-box content-home">
    <section class="sub-header pb-0">
        <div class="block-over-img card-image-only">
            <img class="img-bg img-fluid w-100" src="{{asset('assets/images/pictures/'.$header->name)}}">

            <div class="center-left w-100">
                <div class="text-block">
                    <h2 class="text-capitalize head-title text-white font-600 mb-2">News</h2>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><a href="#">News</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-blank gray-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8 mb-4">
                    <div class="row">
                    @foreach ($news_view as $data )
                        <div class="col-12 col-md-6 mb-4">
                            <div class="card">
                                <div class="block-over-img img-lg">
                                    <img class="card-img-top img-fluid w-100" src="{{asset('assets/images/pictures/'.$data->image)}}">
                                    <div class="top-left">
                                        <span class="badge badge-secondary">{{$data->category_name}}</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <h5 class="text-gold font-title mb-2">{{$data->name}}</h5>
                                    <p class="card-text text-default">{{date("F d, Y",strtotime($data->created_at))}}</p>
                                    <p class="card-text font-300">
                                        {{$data->short_description}}
                                    </p>
                                    <br>
                                    <a href="/frontend/news/detail/{{$data->id}}" class="btn btn-primary" tabindex="0">Read More</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>

                    <br>
                    
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center align-items-center w-100">
                            {{ $news_view->links('vendor.pagination.custom') }}
                        </ul>
                    </nav>
                </div>

                <div class="col-12 col-lg-4 mb-4">
                    <h5 class="text-capitalize head-title mt-0 mb-3">Search</h5>
                    {!! Form::open(['url' => ['frontend/news'],'id'=>'txtsearch','method' => 'POST','enctype' => 'multipart/form-data']) !!}
                    <div class="input-group">
                        <input type="text" name="txtsearch" id="txtsearch" class="form-control"
                                value="{{$search}}" required data-validation-required-message="This field is required">
                        <div class="input-group-append">
                            <button type="submit" class="btn bg-white">
                                <i class="fa fa-search text-secondary"></i>
                            </button>
                        </div>
                    </div>
                     {!! Form::close() !!}

                    <hr class="mt-4 mb-4">

                    <div>
                        <h5 class="text-capitalize head-title mt-0 mb-3">Recent Pos</h5>
                        @foreach ($news_recent as $data )
                        <div class="card-author mb-4">
                            <img src="{{asset('assets/images/pictures/'.$data->image)}}" class="img-fluid img-profile" alt="">
                            <div class="info-profile">
                                <p class="title"><a href="/frontend/news/detail/{{$data->id}}">{{$data->name}}</a></p>
                                <div class="d-flex align-items-center">
                                    <p class="desc mr-3"><a href="/frontend/news/category_news/{{$data->id_category_news}}" class="text-red text-uppercase font-500">{{$data->category_name}}</a></p>
                                    <p class="desc text-uppercase">{{date("F d, Y",strtotime($data->created_at))}}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <hr class="mt-4 mb-4">

                    <div>
                        <h5 class="text-capitalize head-title mt-0 mb-3">Category</h5>
                        <?php 
                            $active_all = '' ;
                            if ($id==0)
                            {
                                $active_all = 'class="active"';
                            }
                        ?>
                        <ul class="category">
                            <li <?php echo $active_all; ?> >
                                <a href="/frontend/news/">
                                    <p>All</p>
                                    <p style="width:30px">{{'( '.$news_count.' )'}}</p>
                                </a>
                            </li>
                            @foreach ($type_news as $data )
                            <li <?php
                                if ($data->id == $id)
                                {
                                    echo "class='active'";
                                }
                                ?> >
                                <a href="/frontend/news/category_news/{{$data->id}}">
                                    <p>{{$data->name}}</p>
                                    <p style="width:30px">{{'( '.$data->sum.' )'}}</p>
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
