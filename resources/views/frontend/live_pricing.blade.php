@extends('layouts.app')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>

@section('content')
<div class="content-box content-home">
    <section class="sub-header pb-0">
        <div class="block-over-img card-image-only">
            <img class="img-bg img-fluid w-100" src="{{asset('assets/images/pictures/'.$header->name)}}">

            <div class="center-left w-100">
                <div class="text-block">
                    <h2 class="text-capitalize head-title text-white font-600 mb-2">Live Pricing</h2>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><a href="#">Live Pricing</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-blank gray-section">
    <center>
        <!-- <div class="container">
            <div class="row mb-2"> -->
                <!-- iframe 
                  width="95%"
                  height="770" frameborder="0" style="overflow:hidden;"
                  src="https://api.royalrafflescapital.com/dashboard_temp"></iframe> -->
                  <iframe src="javascript:;" id="myframe" width="95%"
                  height="770px" frameborder="0" style="overflow:hidden;"></iframe>
            <!-- </div>
        </div> -->
    </center>
    </section>
</div>


<input class="ld-time-input" type="hidden" value="2" id="pollingTime"/>
<input class="ld-url-input" type="hidden" id="fetchURL"/>

<script type="text/javascript">
var defaultData = 'https://demo-live-data.highcharts.com/time-data.csv';
var urlInput = document.getElementById('fetchURL');
//var pollingCheckbox = document.getElementById('enablePolling');
var pollingInput = document.getElementById('pollingTime');

function createChart() {
    Highcharts.chart('container', {
        chart: {
            type: 'spline'
        },
        title: {
            text: 'XAU/IDR'
        },
        xAxis: {
                title: {
                    text: 'Date and Time'
                }
            },
            yAxis: {
                title: {
                    text: 'Exchange Rate'
                }
            },
        data: {
            csvURL: urlInput.value,
            enablePolling: true,
            dataRefreshRate: parseInt(pollingInput.value, 10)
        }
    });

    // if (pollingInput.value < 1 || !pollingInput.value) {
    //     pollingInput.value = 1;
    // }
}

urlInput.value = defaultData;

// We recreate instead of using chart update to make sure the loaded CSV
// and such is completely gone.
//pollingCheckbox.onchange = urlInput.onchange = pollingInput.onchange = createChart;

// Create the chart
createChart();
</script>
@endsection
