@extends('layouts.app')

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

@section('content')
<div class="content-box content-home">
    <section class="sub-header pb-0">
        <div class="block-over-img card-image-only">
            <img class="img-bg img-fluid w-100" src="{{asset('assets/images/pictures/'.$header->name)}}">

            <div class="center-left w-100">
                <div class="text-block">
                    <h2 class="text-capitalize head-title text-white font-600 mb-2">Live Pricing</h2>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><a href="#">Live Pricing</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-blank gray-section">
        <div class="container">
            <div class="row mb-2">
                <div class="col-12 col-lg-6 mb-4">
                    <div class="card">
                        <div class="card-body anual-pricing">
                            <h6 class="font-desc text-gold font-500 mb-3">Buy XAU/IDR</h6>
                            <h3 class="font-desc text-default font-500 mb-0">
                                638, 693, 900.00
                                <span class="status-pricing text-primary"><i class="fa fa-caret-down" aria-hidden="true"></i> 18, 600.00</span>
                            </h3>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-6 mb-4">
                    <div class="card">
                        <div class="card-body anual-pricing">
                            <h6 class="font-desc text-gold font-500 mb-3">Sell XAU/IDR</h6>
                            <h3 class="font-desc text-default font-500 mb-0">
                                634, 772, 700.00
                                <span class="status-pricing text-primary"><i class="fa fa-caret-down" aria-hidden="true"></i> 55, 400.00</span>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
               <div id="container" style="min-width: 1080px; height: 400px; margin: 0 auto"></div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
Highcharts.getJSON(
    'https://cdn.jsdelivr.net/gh/highcharts/highcharts@v7.0.0/samples/data/usdeur.json',
    function (data) {

        Highcharts.chart('container', {
            chart: {
              zoomType: 'x',
              spacingRight: 20
            },
            title: {
                text: 'XAU/IDR'
            },
            subtitle: {
                text: document.ontouchstart === undefined ?
                    'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: 'Exchange rate'
                }
            },
            legend: {
                enabled: false
            },
            exporting: { enabled: false },
            plotOptions: {
                    turboThreshold: 5000,
                    animationLimit: 'infinity',
              area: {
                fillColor: {
                  linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                  stops: [
                    [0, Highcharts.getOptions().colors[0]],
                    [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                  ]
                },
                lineWidth: 1,
                marker: {
                  enabled: false
                },
                shadow: false,
                states: {
                  hover: {
                    lineWidth: 1
                  }
                },
                threshold: null
              }
            },

            series: [{
              type: 'area',
              name: 'USD',
              pointInterval: 10000, //24 * 3600 * 1000
              pointStart: new Date().getTime(),//Date.UTC(2013, 0, 01),
              data: data
            }]
        });
    }
);
</script>
@endsection
