<title>About Us</title>
@extends('layouts.app')

@section('content')
<div class="content-box content-home">
    <section class="sub-header pb-0">
        <div class="block-over-img card-image-only">
            <img class="img-bg img-fluid w-100" src="{{ asset('assets/images/pictures/'.$header->name) }}">

            <div class="center-left w-100">
                <div class="text-block">
                    <h2 class="text-capitalize head-title text-white font-600 mb-2">About Us</h2>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li><a href="#">About Us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="content-blank pb-0">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 mb-4">
                    <div class="header-feature-about">
                        <img src="{{ asset('assets/images/pictures/'.$abouts->image) }}" class="img-fluid" alt="">
                    </div>
                </div>

                <div class="col-12 col-lg-6 mb-4">
                    <div class="about-text-block">
                        <?php
                        	echo $abouts->description;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-blank">
        <div class="container">
            <h3 class="text-capitalize head-title">Our Vision & Mission</h3>
            <p class="head-sub-desc"><?php echo $abouts->vision_mission; ?></p>

            <div class="row m-t-70">
                <div class="col-12 col-lg-6 mb-4">
                    <div class="card-body p-0">
                        <div class="card-center-absolute">
                            <div class="row">
                                <div class="col-12 col-sm-2">
                                    <img src="{{ asset('assets/images/icons/target.png') }}" width="75"
                                        class="img-fluid icon-featured mb-4" alt="">
                                </div>

                                <div class="col-12 col-sm-10">
                                    <div class="card-title text-default mb-2">Vision</div>
                                    <p class="font-300 mb-0">
                                        <?php echo $abouts->vision; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-6 mb-4">
                    <div class="card-body p-0">
                        <div class="card-center-absolute">
                            <div class="row">
                                <div class="col-12 col-sm-2">
                                    <img src="{{ asset('assets/images/icons/startup.png') }}" width="75"
                                        class="img-fluid icon-featured mb-4" alt="">
                                </div>
                                <div class="col-12 col-sm-10">
                                    <div class="card-title text-default mb-2 mb-3">Mission</div>
                                    <p class="font-300 mb-0">
                                        <?php echo $abouts->mission; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="img-about-foot">
        <img src=" {{ asset('assets/images/pictures/'.$subheader->name) }}" class="img-fluid" alt="">
    </div>
</div>
@endsection
