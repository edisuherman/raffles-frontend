@if(session()->has('status_reg'))
    <div class="alert alert-success">
        {{ session()->get('status_reg') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
