<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="image/x-icon" href="{{ asset('assets/images/icons/faveicon.png') }}" rel="shortcut icon"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

   <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}">
    <!-- Slick -->
    <link id="effect" rel="stylesheet" type="text/css" media="all" href="{{ asset('vendor/slick/css/slick.css') }}" />
    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('vendor/slick/css/slick-theme.css') }}" />
    <!-- Main Style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/main.css') }}">
    <style>
    #ProductFilterSection{ order: 1; }
    #ProductContentSection{ order: 2; }
    #ProductPaginationSection{ order: 3; }
    .carousel-item{ height: 100%; }
    .dark-img img { max-height: 100%; }
    </style>
  
</body>
</html>

</head>
<body>
       <div class="top-notif d-none d-md-block">
          <div class="container">
            <div class="content-section">
              <div class="component">
                <p class="text-primary mb-0">
                  <i class="fa fa-phone fa-lg mr-1" aria-hidden="true"></i>
                  <?php
                    echo $company->phone;
                  ?> 
                </p>
              </div>

              <div class="component">
                <p class="text-primary mb-0">
                  <i class="fa fa-envelope fa-lg mr-1" aria-hidden="true"></i>
                  <?php
                    echo $company->email;
                  ?> 
                </p>
              </div>

              <div class="component">
                <ul class="social-media">
                @foreach ($media_social as $data )
                  <li>
                    <a href="{{ $data->link }}" target="_blank" class=""><i class="fa fa-{{ $data->name }}" aria-hidden="true" ></i></a>
                  </li>
                @endforeach 
                </ul>
              </div>
            </div>
          </div>
        </div>

        <nav class="navbar navbar-expand-lg desktop-navigation">
          <div class="container">
            <div class="navbar-header">
              <a class="navbar-brand pt-0" href="/">
                <img src="{{ asset('assets/images/icons/logo-royal.png') }}" alt="" class="img-fluid logo-home">
              </a>
            </div>
            <div id="ShowMenuMobile" class="navbar-toggler">
              <i class="fa fa-bars text-gold" aria-hidden="true"></i>
            </div>

            <?php
              $home='';
              $about='';
              $product='';
              $live='';
              $news='';
              $contact='';
              if ($_SERVER['REQUEST_URI']=='/')
              {
                $home = 'active';
              }

              if ($_SERVER['REQUEST_URI']=='/frontend/about_us/')
              {
                $about = 'active';
              }

              if ($_SERVER['REQUEST_URI']=='/frontend/our_product/')
              {
                $product = 'active';
              }

              if ($_SERVER['REQUEST_URI']=='/frontend/live_pricing/')
              {
                $live = 'active';
              }

              if ($_SERVER['REQUEST_URI']=='/frontend/news/')
              {
                $news = 'active';
              }

              if ($_SERVER['REQUEST_URI']=='/frontend/contact_us/')
              {
                $contact = 'active';
              }

            ?>
            <div class="collapse navbar-collapse" id="navbarDesktop">
              <ul class="navbar-nav mr-auto ml-auto mt-2 mt-lg-0">
                <li class="nav-item <?php echo $home; ?>">
                  <a class="nav-link" href="/">Home</a>
                </li>
                 <li class="nav-item <?php echo $about; ?>">
                  <a class="nav-link" href="/frontend/about_us/">About Us</a>
                </li>
                <li class="nav-item <?php echo $product; ?>">
                  <a class="nav-link" href="/frontend/our_product/">Our Product</a>
                </li>
                <li class="nav-item <?php echo $live; ?>" >
                  <a class="nav-link" href="/frontend/live_pricing/">Live Pricing</a>
                </li>
                <li class="nav-item <?php echo $news; ?>">
                  <a class="nav-link" href="/frontend/news/"/>News</a>
                </li>
                <li class="nav-item <?php echo $contact; ?>">
                  <a class="nav-link" href="/frontend/contact_us/">Contact Us</a>
                </li>
              </ul>
              <ul class="navbar-nav nav-line">
                <li class="nav-item">
                  @if (Auth::check())
                  <a class="nav-link" href="/logout">Logout</a>
                  @else
                  <a class="nav-link" href="/login/">Login</a>
                  @endif
                </li>
                <li class="nav-item pr-0">
                  <a class="nav-link search-icon-btn" href="javascript:void(0)"><i class="fa fa-search"
                      aria-hidden="true"></i></a>
                </li>
              </ul>
            </div>
          </div>
        </nav>

       <!-- <a class="dropdown-item" href="{{ route('logout') }}"
         onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
          {{ __('LOGOUT') }}
      </a>

      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
      </form> -->
      @yield('content')
<!-- Sidebar Menu Mobile -->
<div id="mySidenav" class="sidenav">
    <div class="container-fluid mr-1 ml-1">
        <div class="closebtn text-gold" id="CloseMenuMobile">
            <svg width="15" height="15" viewBox="0 0 16 16">
                <path fill="#FFF" fill-rule="evenodd"
                    d="M16 1.622L9.622 8 16 14.378 14.378 16 8 9.622 1.622 16 0 14.378 6.378 8 0 1.622 1.622 0 8 6.378 14.378 0z">
                </path>
            </svg>
        </div>

        <div class="mt-3 mb-3">
            <img src="{{asset('assets/images/icons/logo-royal.png')}}" width="80" class="img-fluid" alt="">
        </div>

        <ul class="navbar-nav-mobile m-t-40">
            <li class="active">
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/frontend/about_us/">About Us</a>
            </li>
            <li>
                <a href="/frontend/our_product/">Our Product</a>
            </li>
            <li>
                <a href="/frontend/live_pricing/">Live Pricing</a>
            </li>
            <li>
                <a href="/frontend/news/">News</a>
            </li>
            <li>
                <a href="/frontend/contact_us/">Contact Us</a>
            </li>
        </ul>

        <hr>

        <div class="d-flex-center">
            <p class="mb-0"><a href="/login/" class="text-gold text-uppercase">Login</a></p>
            <div class="text-white mr-3 ml-3">|</div>
            <a class="search-icon-btn" href="javascript:void(0)">
                <p class="mb-0"><i class="fa fa-search text-gold" aria-hidden="true"></i></p>
            </a>
        </div>
    </div>

    <div class="foot-contact">
        <p class="text-primary mb-1">
            <i class="fa fa-phone mr-1" aria-hidden="true"></i>
            <?php
                    echo $company->phone;
            ?> 
        </p>

        <p class="text-primary mb-1">
            <i class="fa fa-envelope mr-1" aria-hidden="true"></i>
            <?php
                    echo $company->email;
            ?> 
        </p>

        <ul class="social-media mt-2">
            @foreach ($media_social as $data )
                  <li>
                    <a href="{{ $data->link }}" target="_blank" class=""><i class="fa fa-{{ $data->name }}" aria-hidden="true" ></i></a>
                  </li>
            @endforeach 
        </ul>
    </div>
</div>
</body>
@extends('layouts.footer')
</html>
