@if(session()->has('status_login'))
    <div class="alert alert-danger">
        {{ session()->get('status_login') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif