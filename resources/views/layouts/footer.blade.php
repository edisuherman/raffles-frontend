<!-- Footer -->
<footer>
    <div class="footer-section">
        <div class="container footer-left-xs">
            <div class="row justify-content-between">
                <div class="col-12 col-lg-4 mb-3">
                    <a class="logo" href="/">
                        <img src="{{ asset('assets/images/icons/logo-royal.png') }}" alt="" class="img-fluid">
                    </a>
                    <p class="mt-4">
                        <!-- Lorem ipsum dolor sita met qonqueror dolor sita met qonqueror dolor sita -->
                    </p>
                </div>

                <div class="col-12 col-md-12 col-lg-8 mb-3">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-2">
                            <h4 class="title-list-footer mb-2">Information</h4>
                            <ul class="nav-footer">
                                <li class="nav-item">
                                    <a class="nav-link" href="/">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/frontend/about_us/">About Us</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/frontend/our_product/">Our Product</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/frontend/live_pricing/">Live Pricing</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/frontend/news/">News</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/frontend/contact/">Contact Us</a>
                                </li>
                            </ul>
                        </div>

                        <div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-2">
                            <h4 class="title-list-footer mb-2">Help Center</h4>
                            <ul class="nav-footer">
                            @foreach ($help_center as $data )
                            @if($data->name <>'Privacy Policy' and $data->name <>'Terms and Conditions')
                                <li class="nav-item">
                                    <a class="nav-link" href="/frontend/helpcenter/{{$data->id}}/">{{$data->name}}</a>
                                </li>
                            @endif
                            @endforeach
                            </ul>
                        </div>

                        <div class="col-12 col-md-4 col-lg-4 col-xl-6 mb-2">
                            <h4 class="title-list-footer mb-2">Subscribe Newsletter</h4>
                            <!-- <p>Lorem ipsum dolor sita met qonqueror</p> -->
                            @if(!session()->has('status_login') and !session()->has('status_reg') and !session()->has('status_contact'))

                            @include('layouts.flash')
                            
                            @endif

                            {!! Form::open(['url' => ['frontend/subscribe'],'id'=>'subscribe','method' => 'POST','enctype' => 'multipart/form-data']) !!}

                            <div class="form-group">
                                <input type="email" name="email" class="form-control form-transparent"
                                    placeholder="Write your email" required data-validation-required-message="This field is required">
                            </div>
                            
                            <button type="submit" class="btn btn-secondary w-100">Subscribe</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="copyright-section">
        <div class="container d-flex flex-wrap align-items-center justify-content-between">
            <p class="mt-2 mb-2">
                <?php
                    echo $company->footer;
                ?>
            </p>
            <ul class="list mt-2 mb-2">
                @foreach ($help_center as $data )
                @if($data->name =='Privacy Policy' or $data->name == 'Terms and Conditions')
                <li>
                    <a href="/frontend/helpcenter/{{$data->id}}">
                        <p class="mb-0">{{$data->name}}</p>
                    </a>
                </li>
                @endif
                @endforeach
            </ul>
        </div>
    </div>
</footer>
<!-- Modal Show Search Bar -->
<div id="BlockSearchForm" class="off">
    <div id="search" class="open">
        <div class="container h-100">
            <button data-widget="remove" id="RemoveClass" class="close" type="button">
                <svg width="15" height="15" viewBox="0 0 16 16">
                    <path fill="#4B0000" fill-rule="evenodd"
                        d="M16 1.622L9.622 8 16 14.378 14.378 16 8 9.622 1.622 16 0 14.378 6.378 8 0 1.622 1.622 0 8 6.378 14.378 0z">
                    </path>
                </svg>
            </button>

            <div class="row h-100">
                <div class="col-12 col-lg-8 offset-lg-2">
                    {!! Form::open(['url' => ['frontend/helpcenter'],'class'=>'form-search-section','method' => 'POST','enctype' => 'multipart/form-data']) !!}
                        <h4 class="text-gold mb-3">Search Product or News</h4>
                        <input type="text" placeholder="Type search keywords here..." value="" name="term"
                            id="term">
                        <div class="mt-4">
                            <button type="submit" class="btn btn-primary btn-lg w-100">
                                <i class="fa fa-search mr-1" aria-hidden="true"></i> Search
                            </button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/popper/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/slick/js/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/animsition/js/animsition.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/easyzoom/easyzoom.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/js/main.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/sidebar-mobile.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/add-easyzoom.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/js/slick-add.js') }}"></script>