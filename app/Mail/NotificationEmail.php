<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;

class NotificationEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $name;
    protected $type;
    protected $desc;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $desc, $type)
    {
        $this->name = $name;
        $this->desc = $desc;
        $this->type = $type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $desc = $this->desc;
        $type = $this->type;
        $name = $this->name;
        return $this->subject('Notification '.$type)
                    ->view('frontend.notificationemail')
                    ->with([
                        'name' => $name,
                        'desc' => $desc,
                        'type' => $type
                    ]);
    }
}
