<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Users;

class SendPassword extends Mailable
{
    use Queueable, SerializesModels;

    //protected $fullname;
    //protected $token_actived;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($fullname,$token_actived)
    {
        //dd($fullname);
        $this->fullname = $fullname;
        $this->token_actived = $token_actived;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Users::where('id', $this->user->id)->update([
        //     'mail_status' => 'Sent'
        // ]);
        //dd ($this->fullname);

        return $this->from('trade@royalrafflescapital.com')
                    ->subject('[Royal Raffles Capital] Forget Password ')
                    ->view('frontend.forget_password')
                    ->with([
                        'fullname' => $this->fullname,
                        'token_actived' => $this->token_actived
                    ]);
    }
}
