<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MediaSocial extends Model
{
    protected $table = 'media_social';
}
