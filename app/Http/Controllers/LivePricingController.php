<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use DateTime;
use App\CompanyInfo;
use App\MediaSocial;
use App\HelpCenter;
use App\ImageHeader;

class LivePricingController extends Controller
{
    
    public function index()
    {
      $company = CompanyInfo::first();
      $media_social = MediaSocial::where('status',1)->orderBy('priority', 'asc')->get();
      $help_center = HelpCenter::all();
      $header = ImageHeader::where('menu','live-pricing')
                ->where('type','header')
                ->where('status',1)
                ->first();

      $subheader = ImageHeader::where('menu','live-pricing')
                ->where('type','sub-header')
                ->where('status',1)
                ->first();
      return view('frontend.live_pricing',compact('company','media_social','help_center','header','subheader'));
    }
    
}
