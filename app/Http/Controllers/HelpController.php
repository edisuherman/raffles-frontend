<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use DateTime;
use App\CompanyInfo;
use App\MediaSocial;
use App\HelpCenter;
use App\ImageHeader;

class HelpController extends Controller
{
    
    public function index($id)
    {
	  $help_first = HelpCenter::where('id',$id)->first();
      $company = CompanyInfo::first();
      $media_social = MediaSocial::where('status',1)->orderBy('priority', 'asc')->get();
      $help_center = HelpCenter::all();
      $header = ImageHeader::where('menu','static-page')
                ->where('type','header')
                ->where('status',1)
                ->first();

      $subheader = ImageHeader::where('menu','static-page')
                ->where('type','sub-header')
                ->where('status',1)
                ->first();
      return view('frontend.help',compact('abouts','company','media_social','help_center','help_first','header','subheader'));
    }

    public function searchall(Request $request)
    {
      $company = CompanyInfo::first();
      $media_social = MediaSocial::where('status',1)->orderBy('priority', 'asc')->get();
      $help_center = HelpCenter::all();
      $name_request = $request->input('term');
      $product      =  DB::table('products')
                        ->select('products.*')
                        ->where('name','like','%'.$name_request.'%')
                        ->orWhere('description', 'like','%'.$name_request.'%')
                        ->get();

      $news = DB::table('news')
            ->select('news.*','category_news.name as category_name')
            ->LeftJoin('category_news',function($join){
                $join->on("category_news.id","=","news.id_category_news");
            })
            ->where('news.name','like','%'.$name_request.'%')
            ->orWhere('news.description', 'like','%'.$name_request.'%')
            ->get();

      $count_all = count($product) + count ($news);
      $header = ImageHeader::where('menu','static-page')
                ->where('type','header')
                ->where('status',1)
                ->first();

      $subheader = ImageHeader::where('menu','static-page')
                ->where('type','sub-header')
                ->where('status',1)
                ->first();

      return view('frontend.searchall',compact('abouts','company','media_social','help_center','name_request','product','news','count_all','header','subheader'));
    }
    
}
