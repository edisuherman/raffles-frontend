<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use DateTime;
use App\AboutUs;
use App\CompanyInfo;
use App\MediaSocial;
use App\HelpCenter;
use App\ImageHeader;

class AboutUsController extends Controller
{
    
    public function index()
    {
      $abouts = AboutUs::first();
      $company = CompanyInfo::first();
      $media_social = MediaSocial::where('status',1)->orderBy('priority', 'asc')->get();
      $help_center = HelpCenter::all();
      $header = ImageHeader::where('menu','about-us')
                ->where('type','header')
                ->where('status',1)
                ->first();

      $subheader = ImageHeader::where('menu','about-us')
                ->where('type','sub-header')
                ->where('status',1)
                ->first();
      return view('frontend.about_us',compact('abouts','company','media_social','help_center','header','subheader'));
    }
    
}
