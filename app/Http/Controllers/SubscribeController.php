<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use DateTime;
use App\Subscribe;
use Mail;
use App\Mail\NotificationEmail;
use App\CompanyInfo;

class SubscribeController extends Controller
{
    
    public function index()
    {
	    return view('frontend.home');
    }

    public function store(Request $request)
    {
        $name      = $request->input('email');
        $type      = "Subscribe";
        $desc      = "";
        $company = CompanyInfo::first();
        $email     = $company->email;
	    $subscribe = new Subscribe;
	    $subscribe->email = $request->input('email');
	    $subscribe->save();


        Mail::to($email)->send(new NotificationEmail($name, $desc, $type));
	    
	    $request->session()->flash('status', 'Thank you for subscribe newsletter.');
        return redirect('/');
    }
    
}
