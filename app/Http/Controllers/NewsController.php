<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use DateTime;
use App\TypeNews;
use App\News;
use App\Comments;
use App\CompanyInfo;
use App\MediaSocial;
use App\HelpCenter;
use App\ImageHeader;

class NewsController extends Controller
{
    
    public function index()
    {
      $type_news = DB::table('category_news')
      ->select(array('category_news.id','category_news.name', DB::raw('COUNT(news.id_category_news) as sum')))
      ->LeftJoin('news',function($join){
        $join->on("category_news.id","=","news.id_category_news");
	  })
	  ->groupBy("category_news.id","category_news.name","news.id_category_news")
      ->get();

      $id ='0';

      $news_view = DB::table('news')
            ->select('news.*','category_news.name as category_name')
            ->LeftJoin('category_news',function($join){
                $join->on("category_news.id","=","news.id_category_news");
            })
            ->paginate(6);

      $news_recent = DB::table('news')
            ->select('news.*','category_news.name as category_name')
            ->LeftJoin('category_news',function($join){
                $join->on("category_news.id","=","news.id_category_news");
            })
            ->orderBy('news.id','desc')->take(5)->get();

      $news = News::all();
      $news_count=count($news);
      $company = CompanyInfo::first();
      $media_social = MediaSocial::where('status',1)->orderBy('priority', 'asc')->get();
      $help_center = HelpCenter::all();
      $search='';
      $header = ImageHeader::where('menu','news')
                ->where('type','header')
                ->where('status',1)
                ->first();

      $subheader = ImageHeader::where('menu','news')
                ->where('type','sub-header')
                ->where('status',1)
                ->first();
      return view('frontend.news',compact('type_news','news','company','media_social','help_center','news_count','id','news_view','news_recent','search','header','subheader'));
    }

    public function CategoryNews($id_category_news)
    {
      $type_news = DB::table('category_news')
      ->select(array('category_news.id','category_news.name', DB::raw('COUNT(news.id_category_news) as sum')))
      ->LeftJoin('news',function($join){
        $join->on("category_news.id","=","news.id_category_news");
	  })
	  ->groupBy("category_news.id","category_news.name","news.id_category_news")
      ->get();

      $news_view = DB::table('news')
            ->select('news.*','category_news.name as category_name')
            ->LeftJoin('category_news',function($join){
                $join->on("category_news.id","=","news.id_category_news");
            })
            ->where('news.id_category_news','=',$id_category_news)
            ->paginate(6);

      $news_recent = DB::table('news')
            ->select('news.*','category_news.name as category_name')
            ->LeftJoin('category_news',function($join){
                $join->on("category_news.id","=","news.id_category_news");
            })
            ->orderBy('news.id','desc')->take(5)->get();

      $id =$id_category_news;

      $news = News::all();
      $news_count=count($news);
      $company = CompanyInfo::first();
      $media_social = MediaSocial::where('status',1)->orderBy('priority', 'asc')->get();
      $help_center = HelpCenter::all();
      $search='';
      $header = ImageHeader::where('menu','news')
                ->where('type','header')
                ->where('status',1)
                ->first();

      $subheader = ImageHeader::where('menu','news')
                ->where('type','sub-header')
                ->where('status',1)
                ->first();
      return view('frontend.news',compact('type_news','news','company','media_social','help_center','news_count','id','news_view','news_recent','search','header','subheader'));
    }

    public function search(Request $request)
    {
      $search = $request->input('txtsearch');
      $type_news = DB::table('category_news')
      ->select(array('category_news.id','category_news.name', DB::raw('COUNT(news.id_category_news) as sum')))
      ->LeftJoin('news',function($join){
        $join->on("category_news.id","=","news.id_category_news");
    })
    ->groupBy("category_news.id","category_news.name","news.id_category_news")
      ->get();

      $id ='0';

      $news_view = DB::table('news')
            ->select('news.*','category_news.name as category_name')
            ->LeftJoin('category_news',function($join){
                $join->on("category_news.id","=","news.id_category_news");
            })
            ->where('news.name','like','%'.$search.'%')
            ->orWhere('news.description', 'like','%'.$search.'%')
            ->paginate(6);

      $news_recent = DB::table('news')
            ->select('news.*','category_news.name as category_name')
            ->LeftJoin('category_news',function($join){
                $join->on("category_news.id","=","news.id_category_news");
            })
            ->orderBy('news.id','desc')->take(5)->get();

      $news = News::all();
      $news_count=count($news);
      $company = CompanyInfo::first();
      $media_social = MediaSocial::where('status',1)->orderBy('priority', 'asc')->get();
      $help_center = HelpCenter::all();
      $header = ImageHeader::where('menu','news')
                ->where('type','header')
                ->where('status',1)
                ->first();

      $subheader = ImageHeader::where('menu','news')
                ->where('type','sub-header')
                ->where('status',1)
                ->first();
      return view('frontend.news',compact('type_news','news','company','media_social','help_center','news_count','id','news_view','news_recent','search','header','subheader'));
    }

    public function detail($id)
    {
      $search='';
      $type_news = DB::table('category_news')
      ->select(array('category_news.id','category_news.name', DB::raw('COUNT(news.id_category_news) as sum')))
      ->LeftJoin('news',function($join){
        $join->on("category_news.id","=","news.id_category_news");
      })
      ->groupBy("category_news.id","category_news.name","news.id_category_news")
        ->get();

        $news_view = DB::table('news')
              ->select('news.*','category_news.name as category_name')
              ->LeftJoin('category_news',function($join){
                  $join->on("category_news.id","=","news.id_category_news");
              })
              ->where('news.id','=',$id)
              ->first();

        $news_recent = DB::table('news')
            ->select('news.*','category_news.name as category_name')
            ->LeftJoin('category_news',function($join){
                $join->on("category_news.id","=","news.id_category_news");
            })
            ->orderBy('news.id','desc')->take(5)->get();

        $news_comment = DB::table('comments')
            ->select('comments.*','users.name as fullname', 'users.email as email')
            ->Join('users',function($join){
                $join->on("comments.id_user","=","users.id");
            })
            ->where('comments.id_news','=',$id)
            ->get();




        $news = News::all();
        $news_count=count($news);
        $company = CompanyInfo::first();
        $media_social = MediaSocial::where('status',1)->orderBy('priority', 'asc')->get();
        $help_center = HelpCenter::all();
        $header = ImageHeader::where('menu','news')
                ->where('type','header')
                ->where('status',1)
                ->first();

      $subheader = ImageHeader::where('menu','news')
                ->where('type','sub-header')
                ->where('status',1)
                ->first();
        return view('frontend.news_detail',compact('type_news','news','company','media_social','help_center','news_count','id','news_view','search','news_recent','news_comment','header','subheader'));
    }

    public function store(Request $request)
    {
      $comments = new Comments;
      $comments->id_news = $request->input('id_news');
      $comments->id_user = $request->input('id_user');
      $comments->description = $request->input('message');
      $comments->save();
      
      $request->session()->flash('status_contact', 'Data Has Been Submited');
        return redirect('frontend/news/detail/'.$request->input('id_news'));
    }
    
}
