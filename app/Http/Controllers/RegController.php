<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use DateTime;
use App\CompanyInfo;
use App\MediaSocial;
use App\HelpCenter;
use App\Users;
use App\Subscribe;
use Mail;
use App\Mail\ActivedEmail;
use App\Mail\SendPassword;
use App\Mail\NotificationEmail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegController extends Controller
{
    
    public function index()
    {
      $fullname     = '';
      $email        = '';
      $phone        = '';
      $birthday     = '';
      $address      = '';

      $company = CompanyInfo::first();
      $media_social = MediaSocial::where('status',1)->orderBy('priority', 'asc')->get();
      $help_center = HelpCenter::all();
      return view('auth.register',compact('company','media_social','help_center','fullname','email','phone','birthday','address'));
    }  

    public function store(Request $request)
    {
      $company = CompanyInfo::first();
      $media_social = MediaSocial::where('status',1)->orderBy('priority', 'asc')->get();
      $help_center = HelpCenter::all();

      $is_activated = '0';
      $fullname     = $request->input('fullname');
      $email        = $request->input('email');
      $phone        = $request->input('phone');
      $birthday     = date('Y-m-d',strtotime($request->input('birthday')));
      $address      = $request->input('address');
      $password      = $request->input('password');


      if ($request->input('password')<>$request->input('re_password'))
      {
        $request->session()->flash('status_login', 'Password and Re-Type Password are not the same');
        return view('auth.register',compact('company','media_social','help_center','fullname','email','phone','birthday','address'));
      }

      $cek_user = Users::where('email',$email)->get();

      if (count($cek_user)>0)
      {
        $request->session()->flash('status_login', 'Email is not avalaible');
        return view('auth.register',compact('company','media_social','help_center','fullname','email','phone','birthday','address'));
      }

      if (strlen($password)<5)
      {
        $request->session()->flash('status_login', 'Length Password min 6 character');
        return view('auth.register',compact('company','media_social','help_center','fullname','email','phone','birthday','address'));
      }

      if($request->input('chk_subscribe')=='on')
      {
        $subscribe = new Subscribe;
        $subscribe->email = $email;
        $subscribe->save();
      }

      $token_actived = md5(microtime());

      $users = new Users;
      $users->name = $fullname;
      $users->email = $email;
      $users->password = Hash::make($request->input('password'));
      $users->phone = $phone ;
      $users->birthday = $birthday;
      $users->address = $address;
      $users->is_activated = $is_activated;
      $users->token_actived = $token_actived;
      $users->save();

      $name      = $fullname;
      $type      = "New User";
      $desc      = "";
      $company   = CompanyInfo::first();
      $email_to  = $company->email;

      Mail::to($email_to)->send(new NotificationEmail($name, $desc, $type));

      //'password' => Hash::make($data['password']),
      $request->session()->flash('status_reg', 'Data Has Been Submited. Please Wait For Actived.');
        return redirect('/login');
    } 

    public function token($id)
    {
      $token_actived = $id;
      $company = CompanyInfo::first();
      $media_social = MediaSocial::where('status',1)->orderBy('priority', 'asc')->get();
      $help_center = HelpCenter::all();

      $token = DB::table('users')->where('token_password',$id)->first();
      //dd($token);

      if($token)
      {
        return view('auth.new_password',compact('company','media_social','help_center','token_actived'));
      }
      else
      {
        session()->flash('status_login', 'Account Not Found');
        return redirect('/login');
      }
    }

    public function forgot_password()
    {
      $company = CompanyInfo::first();
      $media_social = MediaSocial::where('status',1)->orderBy('priority', 'asc')->get();
      $help_center = HelpCenter::all();
      return view('auth.forgot_password',compact('company','media_social','help_center'));
    }  

    public function send_password(Request $request)
    {
      //dd($request->email);
      //Mail::to($email)->send(new ActivedEmail($fullname, $token_actived));
      $email = $request->email;
      $company = CompanyInfo::first();
      $media_social = MediaSocial::where('status',1)->orderBy('priority', 'asc')->get();
      $help_center = HelpCenter::all();

      $token_actived = md5(microtime());
      
      $token = DB::table('users')
               ->where('email',$email)
               ->where('is_activated','1')
               ->first();
      
      if($token)
      {
        $users = DB::table('users')
            ->where('email', $email)
            ->update(['token_password' => $token_actived]);

        $fullname=$token->name;

        Mail::to($email)->send(new SendPassword($fullname, $token_actived));

        session()->flash('status_reg', 'Token Has Send To Your Email. Please Check Your Email.');
      }
      else
      {
        session()->flash('status_login', 'Account Not Found or Not Active');
      }


      return view('auth.forgot_password',compact('company','media_social','help_center'));
    }

    public function storepassword(Request $request)
    {
      $company = CompanyInfo::first();
      $media_social = MediaSocial::where('status',1)->orderBy('priority', 'asc')->get();
      $help_center = HelpCenter::all();

      if (strlen($request->input('password'))<5)
      {
        return back()->with('failed', 'Length Password min 6 character');
      }


      if ($request->input('password')<>$request->input('conf_password'))
      {
        return back()->with('failed', 'Password and Re-Type Password are not the same');
      }
      $token_actived = $request->input('token_actived');
      $password = Hash::make($request->input('password'));
      $users = DB::table('users')
            ->where('token_password', $token_actived)
            ->update(['password' => $password]);
      
      session()->flash('status_reg', 'Password Has Change');
      return redirect('/login');
    }      
}
