<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use App\CompanyInfo;
use App\MediaSocial;
use App\HelpCenter;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */


    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/frontend/live_pricing';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {

        
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_activated' => 1])) {
             return redirect()->intended('/frontend/live_pricing/');
        }  elseif (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            Auth::logout();
            $request->session()->flash('status_login', 'This account is not activated');
            return redirect('/login');
        } else{
            $request->session()->flash('status_login', 'These credentials do not match our records.');
            return redirect('/login');
        }


    }

    protected function credentials(Request $request)
        {
          if(is_numeric($request->get('email'))){
            return ['nip'=>$request->get('email'),'password'=>$request->get('password')];
          }
          elseif (filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)) {
            return ['email' => $request->get('email'), 'password'=>$request->get('password')];
          }
        }

    public function showLoginForm()
    {
        $company = CompanyInfo::first();
        $media_social = MediaSocial::where('status',1)->orderBy('priority', 'asc')->get();
        $help_center = HelpCenter::all();
        return view('auth.login',compact('company','media_social','help_center'));
    }

    protected function attemptLogin(Request $request)
    {

        return $this->guard()->attempt(
            $this->credentials($request), $request->has('remember')
        );
    }


    public function logout(Request $request) {
      Auth::logout();
      return redirect('/home');
    }
}
