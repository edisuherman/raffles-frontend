<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use DateTime;
use App\CompanyInfo;
use App\Complain;
use App\MediaSocial;
use App\HelpCenter;
use App\ImageHeader;
use Mail;
use App\Mail\NotificationEmail;

class CompanyInfoController extends Controller
{
    
    public function index()
    {
	    $company = CompanyInfo::first();
	    $media_social = MediaSocial::where('status',1)->orderBy('priority', 'asc')->get();
	    $help_center = HelpCenter::all();
	    $header = ImageHeader::where('menu','contact-us')
                ->where('type','header')
                ->where('status',1)
                ->first();

      	$subheader = ImageHeader::where('menu','contact-us')
                ->where('type','sub-header')
                ->where('status',1)
                ->first();
	    return view('frontend.contact_us',compact('company','media_social','help_center','header','subheader'));
    }

    public function store(Request $request)
    {
	    $complain = new Complain;
        $type      = "Complain or Question";
        $company = CompanyInfo::first();
        $email     = $company->email;
        $desc      = "";
        $name      = $request->input('fullname');
	    $complain->fullname = $request->input('fullname');
	    $complain->email = $request->input('email');
	    $complain->subject = $request->input('subject');
	    $complain->message = $request->input('message');
	    $complain->save();

        Mail::to($email)->send(new NotificationEmail($name, $desc, $type));
	    
	    $request->session()->flash('status_contact', 'Data Has Been Submited');
        return redirect('frontend/contact_us');
    }
    
}
