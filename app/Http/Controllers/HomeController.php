<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use DateTime;
use App\TypeProduct;
use App\Product;
use App\CompanyInfo;
use App\MediaSocial;
use App\Slider;
use App\HelpCenter;

class HomeController extends Controller
{
    
    public function index()
    {
	    $company = CompanyInfo::first();
	    $media_social = MediaSocial::where('status',1)->orderBy('priority', 'asc')->get();
	    $help_center = HelpCenter::all();
	    $type_product = TypeProduct::all();
	    $product = DB::table('products')->OrderBy('id','desc')->get();
	    $slider = DB::table('sliders')->OrderBy('id','desc')->take(3)->get();
	    $news_recent = DB::table('news')
            ->select('news.*','category_news.name as category_name')
            ->LeftJoin('category_news',function($join){
                $join->on("category_news.id","=","news.id_category_news");
            })
            ->orderBy('news.id','desc')->take(10)->get();
	    return view('frontend.home',compact('company','media_social','help_center','news_recent','type_product','product','slider'));
    }
    
}
