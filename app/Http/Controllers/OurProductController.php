<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use DateTime;
use App\TypeProduct;
use App\Product;
use App\CompanyInfo;
use App\MediaSocial;
use App\HelpCenter;
use App\ImageHeader;

class OurProductController extends Controller
{
    
    public function index()
    {
      $type_product = TypeProduct::all();
      //$product = Product::all();
      $product = DB::table('products')->paginate(6);
      $company = CompanyInfo::first();
      $media_social = MediaSocial::where('status',1)->orderBy('priority', 'asc')->get();
      $help_center = HelpCenter::all();
      $vtp = "";
      $id = "";
      $category_name = "All Category";
      $search_text="";
      $id_sorting ='';
      $header = ImageHeader::where('menu','our-product')
                ->where('type','header')
                ->where('status',1)
                ->first();

      $subheader = ImageHeader::where('menu','our-product')
                ->where('type','sub-header')
                ->where('status',1)
                ->first();
      return view('frontend.our_product',compact('type_product','product','company','media_social','help_center','vtp','category_name','id','search_text','id_sorting','header','subheader'));
    }

    public function category($category)
    {
      $pieces = explode("-", $category);
      $category_name    = $pieces[0]; // piece1
      $vtp    = str_replace(' ','',$pieces[0]); // piece1
      $id     = $pieces[1]; // piece2
      $type_product = TypeProduct::all();
      $product = DB::table('products')->where('id_category',$id)->paginate(6);
      $company = CompanyInfo::first();
      $media_social = MediaSocial::where('status',1)->orderBy('priority', 'asc')->get();
      $help_center = HelpCenter::all();
      $search_text="";
      $id_sorting ='';
      $header = ImageHeader::where('menu','our-product')
                ->where('type','header')
                ->where('status',1)
                ->first();

      $subheader = ImageHeader::where('menu','our-product')
                ->where('type','sub-header')
                ->where('status',1)
                ->first();
      return view('frontend.our_product',compact('type_product','product','company','media_social','help_center','vtp','category_name','id','search_text','id_sorting','header','subheader'));
    }

    public function search(Request $request)
    {
      $search = $request->input('txtsearch');
      $category_name = "All Category";
      $vtp = "";
      $id = "";
      $type_product = TypeProduct::all();
      $product = DB::table('products')->where('name','like','%'.$search.'%')->orWhere('description', 'like','%'.$search.'%')->paginate(6);

      $product_count =  DB::table('products')
                        ->select('id_category', DB::raw('count(id_category) as jml'))
                        ->where('name','like','%'.$search.'%')
                        ->orWhere('description', 'like','%'.$search.'%')
                        ->groupBy('id_category')
                        ->get();

      if(COUNT($product_count)==1)
      {
        foreach ($product_count as $data){
            $id = $data->id_category;}
        $type_product_name = DB::table('category_products')->where('id',$id)->first();
        $vtp = str_replace(' ','',$type_product_name->name);
        $category_name = $type_product_name->name;
      }

      $company = CompanyInfo::first();
      $media_social = MediaSocial::where('status',1)->orderBy('priority', 'asc')->get();
      $help_center = HelpCenter::all();
      $search_text=$search ;
      $id_sorting ='';
      $header = ImageHeader::where('menu','our-product')
                ->where('type','header')
                ->where('status',1)
                ->first();

      $subheader = ImageHeader::where('menu','our-product')
                ->where('type','sub-header')
                ->where('status',1)
                ->first();
      return view('frontend.our_product',compact('type_product','product','company','media_social','help_center','vtp','category_name','id','search_text','id_sorting','header','subheader'));
    }

    public function detail($id)
    {
      $product = Product::find($id);
      $product_all = Product::all();
      $company = CompanyInfo::first();
      $media_social = MediaSocial::where('status',1)->orderBy('priority', 'asc')->get();
      $help_center = HelpCenter::all();
      $header = ImageHeader::where('menu','our-product')
                ->where('type','header')
                ->where('status',1)
                ->first();

      $subheader = ImageHeader::where('menu','our-product')
                ->where('type','sub-header')
                ->where('status',1)
                ->first();
      return view('frontend.our_product_detail',compact('product','product_all','company','media_social','help_center','header','subheader'));
    }

    public function OrderAll($id)
    {
      $link_search = explode("_", $id);
      $ids = $link_search[0];
      $find = '';
      if (count($link_search)>1)
      {
        $find = str_replace('%20',' ',$link_search[1]);
      }

      if ($find=='Type Keyword')
      {
        $find = '';
      }

      $type_product = TypeProduct::all();
      $product = DB::table('products')->paginate(6);

      if ($ids==0)
      {
        $product = DB::table('products')
        ->where(function($q) use ($find) {
          $q->where('name','like','%'.$find.'%')
            ->orWhere('description', 'like','%'.$find.'%');
          })
        ->orderBy('id', 'asc')
        ->paginate(6);
      }

      if ($ids==1)
      {
        $product = DB::table('products')
        ->where(function($q) use ($find) {
          $q->where('name','like','%'.$find.'%')
            ->orWhere('description', 'like','%'.$find.'%');
          })
        ->orderBy('id', 'desc')
        ->paginate(6);
      }

      if ($ids==2)
      {
        $product = DB::table('products')
        ->where(function($q) use ($find) {
          $q->where('name','like','%'.$find.'%')
            ->orWhere('description', 'like','%'.$find.'%');
          })
        ->orderBy('price', 'asc')
        ->paginate(6);
      }

      if ($ids==3)
      {
        $product = DB::table('products')
        ->where(function($q) use ($find) {
          $q->where('name','like','%'.$find.'%')
            ->orWhere('description', 'like','%'.$find.'%');
          })
        ->orderBy('price', 'desc')
        ->paginate(6);
      }

      $company = CompanyInfo::first();
      $media_social = MediaSocial::where('status',1)->orderBy('priority', 'asc')->get();
      $help_center = HelpCenter::all();
      $vtp = "";
      $id = "";
      $category_name = "All Category";
      $search_text=$find;
      $id_sorting =$ids;
      $header = ImageHeader::where('menu','our-product')
                ->where('type','header')
                ->where('status',1)
                ->first();

      $subheader = ImageHeader::where('menu','our-product')
                ->where('type','sub-header')
                ->where('status',1)
                ->first();
      return view('frontend.our_product',compact('type_product','product','company','media_social','help_center','vtp','category_name','id','search_text','id_sorting','header','subheader'));
    }

    public function OrderPartial($category,$id)
    {
      $link_search = explode("_", $id);
      $ids = $link_search[0];
      $find = '';
      if (count($link_search)>1)
      {
        $find = str_replace('%20',' ',$link_search[1]);
      }

      if ($find=='Type Keyword')
      {
        $find = '';
      }

      $pieces = explode("-", $category);
      $category_name    = $pieces[0]; // piece1
      $vtp    = str_replace(' ','',$pieces[0]); // piece1
      $id     = $pieces[1]; // piece2
      $type_product = TypeProduct::all();
      $product = DB::table('products')->where('id_category',$id)->paginate(6);

      if ($ids==0)
      {
        $product = DB::table('products')
        ->where(function($q) use ($find) {
          $q->where('name','like','%'.$find.'%')
            ->orWhere('description', 'like','%'.$find.'%');
          })
        ->orderBy('id', 'asc')
        ->paginate(6);
      }

      if ($ids==1)
      {
        $product = DB::table('products')->where('id_category',$id)
        ->where(function($q) use ($find) {
          $q->where('name','like','%'.$find.'%')
            ->orWhere('description', 'like','%'.$find.'%');
          })
        ->orderBy('id', 'desc')
        ->paginate(6);
      }

      if ($ids==2)
      {
        $product = DB::table('products')->where('id_category',$id)
        ->where(function($q) use ($find) {
          $q->where('name','like','%'.$find.'%')
            ->orWhere('description', 'like','%'.$find.'%');
          })
        ->orderBy('price', 'asc')
        ->paginate(6);
      }

      if ($ids==3)
      {
        $product = DB::table('products')->where('id_category',$id)
        ->where(function($q) use ($find) {
          $q->where('name','like','%'.$find.'%')
            ->orWhere('description', 'like','%'.$find.'%');
          })
        ->orderBy('price', 'desc')
        ->paginate(6);
      }


      $company = CompanyInfo::first();
      $media_social = MediaSocial::where('status',1)->orderBy('priority', 'asc')->get();
      $help_center = HelpCenter::all();
      $search_text=$find;
      $id_sorting =$ids;
      $header = ImageHeader::where('menu','our-product')
                ->where('type','header')
                ->where('status',1)
                ->first();

      $subheader = ImageHeader::where('menu','our-product')
                ->where('type','sub-header')
                ->where('status',1)
                ->first();
      return view('frontend.our_product',compact('type_product','product','company','media_social','help_center','vtp','category_name','id','search_text','id_sorting','header','subheader'));
    }
    
}
