<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageHeader extends Model
{
    protected $table = 'image_header';
}
