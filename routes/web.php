<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','HomeController@index');

//Route::get('login/news','LoginNewsController@index');

Route::get('/home','HomeController@index');

Route::get('reg','RegController@index');
Route::post('reg','RegController@store');
Route::get('verificationpassword/{id}','RegController@token');
Route::get('forgot_password','RegController@forgot_password');
Route::post('send_password','RegController@send_password');
Route::post('new_password','RegController@storepassword');



Auth::routes();

Route::group(['prefix' => 'frontend'], function() {
    Route::get('about_us','AboutUsController@index');

	Route::get('our_product','OurProductController@index');
	Route::get('our_product/{category}','OurProductController@category');
    Route::get('our_product/detail/{id}','OurProductController@detail');
    Route::post('our_product','OurProductController@search');
    Route::get('our_product/order/{id_order}','OurProductController@OrderAll');
    Route::get('our_product/{category}/order/{id_order}','OurProductController@OrderPartial');

    Route::get('news','NewsController@index');
    Route::get('news/category_news/{id_category_news}','NewsController@CategoryNews');
    Route::post('news','NewsController@search');
    Route::post('news/comment','NewsController@store');
    Route::get('news/detail/{id}','NewsController@detail');

    Route::get('helpcenter/{id}','HelpController@index');
    Route::post('helpcenter','HelpController@searchall');


    Route::get('contact_us','CompanyInfoController@index');
    Route::post('contact_us','CompanyInfoController@store');
    Route::post('subscribe','SubscribeController@store');
});

Route::group(['prefix' => 'frontend', 'middleware' => 'auth','isVerified'], function () {

   Route::get('live_pricing','LivePricingController@index');

});

Route::get('logout','\App\Http\Controllers\Auth\LoginController@logout');
